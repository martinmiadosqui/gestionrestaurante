package com.gestionResto.apiResto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiRestoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRestoApplication.class, args);
	}

}
